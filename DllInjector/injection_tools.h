#pragma once
#include <windows.h>
#include <subauth.h>

#ifdef  _WIN64

static const UCHAR code1[] = {
	0x55,                   // push rbp
	0x48, 0x89, 0xE5,       // mov  rbp,rsp
	0x48, 0x83, 0xEC, 0x20, // sub  rsp,byte +0x28
	0x48, 0xB9              // mov  rcx,<LibName>
};

static const UCHAR code2[] = {
	0x48, 0xBA              // mov rdx,<LoadLibrary>
};


static const UCHAR code3[] = {
	0xFF, 0xD2,             // call rdx
	0x48, 0x85, 0xC0,       // test rax,rax
	0x74, 0x05,             // jz proc_failure
	0x48, 0x31, 0xC0,       // xor rax,rax
	0xEB, 0x0C,             // jmp proc_end
							// proc_failure:
							0x48, 0xBA              // mov rdx,<GetLastError>
};

static const UCHAR code4[] = {
	0xFF, 0xD2,             // call rdx
							// proc_end:
							0x48, 0x83, 0xC4, 0x20, // add rsp,byte +0x28
							0x5D,                   // pop rbp
							0xC2, 0x08, 0x00        // ret 0x8
};

static const UCHAR get_ioml_code[] = {
	0x55,                                         // push rbp
	0x48, 0x89, 0xE5,							  // mov rbp,rsp
	0x65, 0x48, 0x8B, 0x04, 0x25, 0x60, 0, 0, 0,  // mov rax,[gs:0x60]  ; PEB
	0x48, 0x8B, 0x40, 0x18,                       // mov rax,[rax+0x18] ; PEB_LDR_DATA
	0x48, 0x8B, 0x40, 0x20,                       // mov rax,[rax+0x20] ; InitializationOrderModuleList
	0x48, 0x8B, 0,                                // mov rax,[rax]
	0x48, 0x83, 0xC0, 0x10,                       // add rax, 0x10
	0x48, 0x89, 0x01,                             // mov [rcx],rax      ; save to out param
	0x5D,                                         // pop rbp
	0xC2, 0x08, 0                                 // ret 0x8
};

#else // i386

static const UCHAR code1[] = {
	0x55,         // push ebp
	0x89, 0xE5,   // mov ebp,esp
	0x68          // push <libName>
};

static const UCHAR code2[] = {
	0xB9          // mov ecx, <LoadLibrary>
};

static const UCHAR code3[] = {
	0xFF, 0xD1,   // call ecx
	0x85, 0xC0,   // test eax,eax
	0x74, 0x04,   // jz 0x17 <proc_failure>
	0x31, 0xC0,   // xor eax,eax
	0xEB, 0x07,   // jmp short 0x1e <proc_end>
				  // <proc_failure:>
				  0xB9          // mov ecx, <GetLastError>
};

static const UCHAR code4[] = {
	0xFF, 0xD1,    // call ecx
				   // <proc_end:>
				   0x5D,          // pop ebp
				   0xC2, 0x04, 0  // ret 0x4
};

static const UCHAR get_ioml_code[] = {
	0x55,                      // push ebp
	0x89, 0xE5,                // mov  ebp,esp
	0x64, 0xA1, 0x30, 0, 0, 0, // mov  eax,[fs:0x30]  ; PEB
	0x8B, 0x40, 0x0C,          // mov  eax,[eax+0xc]  ; PEB_LDR_DATA
	0x8B, 0x40, 0x1C,          // mov  eax,[eax+0x1c] ; InitializationOrderModuleList
	0x8B, 0x4D, 0x08,          // mov  ecx,[ebp+0x8]  ; func argument
	0x89, 0x01,                // mov  [ecx],eax      ; save to out param
	0x5D,                      // pop  ebp
	0xC2, 0x04, 0              // ret  0x4
};

#endif

#pragma pack(push,1)
typedef struct {
	UCHAR		code1[sizeof(code1)];
	ULONG_PTR	pLibName;
	UCHAR		code2[sizeof(code2)];
	ULONG_PTR	pLoadLibrary;
	UCHAR		code3[sizeof(code3)];
	ULONG_PTR	pGetLastError;
	UCHAR		code4[sizeof(code4)];
	UCHAR		libName[MAX_PATH + 1];
} shellcode_t;
#pragma pack(pop)


typedef struct LDR_DATA_ENTRY {
	LIST_ENTRY              InMemoryOrderModuleList;
	PVOID                   BaseAddress;
	PVOID                   EntryPoint;
	ULONG                   SizeOfImage;
	UNICODE_STRING          FullDllName;
	UNICODE_STRING          BaseDllName;
} LDR_DATA_ENTRY, *PLDR_DATA_ENTRY;