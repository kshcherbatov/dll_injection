#include "RunDllInjectedThread.h"
#include "debug_tools.h"
#include "injection_tools.h"


static int RunMembufferRemote(HANDLE hProcess, LPVOID memBuffer, SIZE_T memBufferSize, ULONG_PTR arg, OUT LPDWORD exitCode)
{
	int ret = 0;

	DWORD oldPermissions;
	if (!VirtualProtectEx(hProcess, memBuffer, memBufferSize, PAGE_EXECUTE_READWRITE, &oldPermissions)) {
		ErrMsg("VirtualProtectEx");
		return -1;
	}

	HANDLE hInjectedThread = INVALID_HANDLE_VALUE;
	if (!(hInjectedThread = CreateRemoteThread(hProcess, NULL, 0, (LPTHREAD_START_ROUTINE)memBuffer, (LPVOID)arg, 0, NULL))) {
		// access denied - invalid architecture
		ErrMsg("CreateRemoteThread");
		ret = -1;
		goto __end;
	}

	DWORD waitRes = WaitForSingleObject(hInjectedThread, INFINITE);
	switch (waitRes) {
	case WAIT_OBJECT_0: {
		if (exitCode)
			if (!GetExitCodeThread(hInjectedThread, exitCode)) {
				ErrMsg("GetExitCodeThread");
				ret = -1;
			}
		break;
	}
	case WAIT_FAILED:
		ErrMsg("WaitForSingleObject");
		ret = -1;
		break;
	default:
		DOUT("Strange WaitForSingleObject behavior: %d\n", (int)waitRes);
		ret = -1;
		break;
	}

__end:
	return ret;
}


static int FindDllBaseRemote(HANDLE hProcess, LPVOID pLDR, LPCWSTR dllName, OUT PULONG_PTR pDwBaseAddress)
{
	*pDwBaseAddress = 0;

	PLDR_DATA_ENTRY cursor = pLDR;
	LDR_DATA_ENTRY ldrDataEntry;
	SIZE_T bytesReaded;

	if (!ReadProcessMemory(hProcess, (LPVOID)cursor, &ldrDataEntry, sizeof(LDR_DATA_ENTRY), &bytesReaded) ||
		bytesReaded != sizeof(LDR_DATA_ENTRY)) {
		ErrMsg("ReadProcessMemory");
		return -1;
	}

	while (ldrDataEntry.BaseAddress) {
		WCHAR foreignDllName[MAX_PATH + 1];
		SIZE_T foreignDllNameSize = ldrDataEntry.BaseDllName.Length;

		if (!ReadProcessMemory(hProcess, ldrDataEntry.BaseDllName.Buffer, foreignDllName, foreignDllNameSize, &bytesReaded) ||
			bytesReaded != foreignDllNameSize) {
			ErrMsg("ReadProcessMemory");
			return -1;
		}

		foreignDllName[foreignDllNameSize / sizeof(*foreignDllName)] = 0x0;

		if (!_wcsicmp(foreignDllName, dllName)) {
			*pDwBaseAddress = (ULONG_PTR)ldrDataEntry.BaseAddress;
			return 0;
		}

		cursor = (PLDR_DATA_ENTRY)ldrDataEntry.InMemoryOrderModuleList.Flink;
		if (!ReadProcessMemory(hProcess, cursor, &ldrDataEntry, sizeof(LDR_DATA_ENTRY), &bytesReaded) ||
			bytesReaded != sizeof(LDR_DATA_ENTRY)) {
			ErrMsg("ReadProcessMemory");
			return -1;
		}
	}
	return -1;
}


static int GetFunctionAddrRemote(HANDLE hProcess, ULONG_PTR dwModuleBase, LPCWSTR moduleName, LPCSTR funcName, OUT PULONG_PTR pFunc)
{
	HANDLE myModule = GetModuleHandle(moduleName);
	if (!myModule) {
		ErrMsg("GetModuleHandle");
		return -1;
	}
	LPVOID myFunc = GetProcAddress(myModule, funcName);
	if (!myFunc) {
		ErrMsg("GetProcAddress");
		return -1;
	}

	*pFunc = dwModuleBase + ((ULONG_PTR)myFunc - (ULONG_PTR)myModule);
	return 0;
}


static int GetPLDR(HANDLE hProcess, OUT PULONG_PTR ppLDR)
{
	*ppLDR = 0;

	int ret = 0;

	LPVOID getPLdrCodeRemote = NULL;
	SIZE_T getPLdrCodeRemoteSize = sizeof(get_ioml_code) + sizeof(ULONG_PTR); // Pointer at the end - result

	if (!(getPLdrCodeRemote = VirtualAllocEx(hProcess, NULL, getPLdrCodeRemoteSize, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE))) {
		ErrMsg("VirtualAllocEx");
		return -1;
	}
	PULONG_PTR getPLdrCodeRemoteResult = (PULONG_PTR)((ULONG_PTR)getPLdrCodeRemote + sizeof(get_ioml_code));

	if (!WriteProcessMemory(hProcess, getPLdrCodeRemote, get_ioml_code, getPLdrCodeRemoteSize, NULL)) {
		ErrMsg("WriteProcessMemory");
		ret = -1;
		goto __end;
	}
	if (RunMembufferRemote(hProcess, getPLdrCodeRemote, sizeof(get_ioml_code), (ULONG_PTR)getPLdrCodeRemoteResult, NULL)) {
		DOUT("RunMembufferRemote failed for (hProcess=[%p], getPLdrCodeRemote=[%p], size=%llu, dest=[%p], exitCode=[%p])\n",
			hProcess, getPLdrCodeRemote, sizeof(get_ioml_code), (LPVOID)getPLdrCodeRemoteResult, NULL);
		ret = -1;
		goto __end;
	}

	SIZE_T bytesReaded;
	if (!ReadProcessMemory(hProcess, getPLdrCodeRemoteResult, ppLDR, sizeof(*getPLdrCodeRemoteResult), &bytesReaded) ||
		bytesReaded != sizeof(*getPLdrCodeRemoteResult)) {
		ErrMsg("ReadProcessMemory");
		ret = -1;
		goto __end;
	}

__end:
	if (!VirtualFreeEx(hProcess, getPLdrCodeRemote, 0, MEM_RELEASE))
		ErrMsg("VirtualFreeEx");

	return ret;
}


static int GetLoadLibraryAddrRemote(HANDLE hProcess, OUT PULONG_PTR pLoadLibrary, OUT PULONG_PTR pGetLastError)
{
	*pLoadLibrary = 0;
	*pGetLastError = 0;

	int ret = 0;

	LPVOID pLDR;
	if (GetPLDR(hProcess, (PULONG_PTR)&pLDR)) {
		DOUT("GetPLDR failed for (hProcess=[%p], ppLDR=[%p])\n", hProcess, &pLDR);
		ret = -1;
		goto __end;
	}

	ULONG_PTR kernel32BaseAddr;
	LPCWSTR libName = L"Kernel32.dll";
	if (FindDllBaseRemote(hProcess, pLDR, libName, &kernel32BaseAddr)) {
		DOUT("FindDllBaseRemote failed for (hProcess=[%p], pLDR=[%p], libName={%S}, pDwBaseAddress=[%p])\n",
			hProcess, pLDR, libName, &kernel32BaseAddr);
		ret = -1;
		goto __end;
	}

	DOUT("Obtained Kernel32 BaseAddr [%p]\n", (LPVOID)kernel32BaseAddr);
	assert(kernel32BaseAddr);

	if (GetFunctionAddrRemote(hProcess, kernel32BaseAddr, libName, "LoadLibraryA", pLoadLibrary) ||
		GetFunctionAddrRemote(hProcess, kernel32BaseAddr, libName, "GetLastError", pGetLastError) ||
		!pLoadLibrary || !pGetLastError
		) {
		DOUT("GetFunctionAddrRemote failed for (hProcess=[%p], kernel32BaseAddr=[%p], ...)\n",
			hProcess, (LPCVOID)kernel32BaseAddr);
		ret = -1;
		goto __end;
	}

__end:
	return ret;
}


static int InitShellcodeRemote(HANDLE hProcess, LPCSTR libName, ULONG_PTR pLoadLibrary, ULONG_PTR pGetLastError, OUT PULONG_PTR virtMem)
{
	assert(virtMem);

	shellcode_t shellcode;
	ULONG_PTR pLibName = (ULONG_PTR)virtMem + sizeof(shellcode_t) - (MAX_PATH + 1);

	memcpy(&shellcode.code1, code1, sizeof(code1));
	memcpy(&shellcode.code2, code2, sizeof(code2));
	memcpy(&shellcode.code3, code3, sizeof(code3));
	memcpy(&shellcode.code4, code4, sizeof(code4));
	memcpy(&shellcode.libName, libName, strlen(libName) + 1);
	shellcode.pLibName = pLibName;
	shellcode.pLoadLibrary = pLoadLibrary;
	shellcode.pGetLastError = pGetLastError;

	if (!WriteProcessMemory(hProcess, virtMem, &shellcode, sizeof(shellcode_t), NULL)) {
		ErrMsg("WriteProcessMemory");
		return -1;
	}
	return 0;
}


static int StartSuspendedProcess(LPCSTR prgName, OUT LPPROCESS_INFORMATION pi)
{
	STARTUPINFOA cif;
	ZeroMemory(&cif, sizeof(STARTUPINFO));

	if (!CreateProcessA(prgName, NULL, NULL, NULL, FALSE, CREATE_SUSPENDED, NULL, NULL, &cif, pi)) {
		ErrMsg("CreateProcess");
		return -1;
	}

	return 0;
}


int RunLibraryInjectedProcess(LPCSTR prgName, LPCSTR dllName)
{
	int ret = 0;

	HANDLE hProcess = INVALID_HANDLE_VALUE;
	HANDLE hThread = INVALID_HANDLE_VALUE;
	BOOL masterThreadCase = FALSE;

	if (prgName) {
		PROCESS_INFORMATION pi;
		if (StartSuspendedProcess(prgName, &pi)) {
			DOUT("StartSuspendedProcess failed for (prgName={%s}, ppi=[%p])\n", prgName, &pi);
			return -1;
		}
		hProcess = pi.hProcess;
		hThread = pi.hThread;
	}
	else {
		hProcess = GetCurrentProcess();
		masterThreadCase = TRUE;
	}

	ULONG_PTR pLoadLibrary, pGetLastError;
	if (GetLoadLibraryAddrRemote(hProcess, &pLoadLibrary, &pGetLastError)) {
		DOUT("GetLoadLibraryAddrRemote failed for (hProcess=[%p], ppLoadLibrary=[%p], ppGetLastError=[%p])\n",
			hProcess, &pLoadLibrary, &pGetLastError);
		ret = -1;
		goto __err_proc_free;
	}

	DOUT("Obtained remote pLoadLibrary = [%p], pGetLastError = [%p]\n",
		(LPCVOID)pLoadLibrary, (LPCVOID)pGetLastError);

	LPVOID injectLibraryCodeRemote;
	if (!(injectLibraryCodeRemote = VirtualAllocEx(hProcess, NULL, sizeof(shellcode_t), MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE))) {
		ErrMsg("VirtualAllocEx");
		ret = -1;
		goto __err_proc_free;
	}

	if (InitShellcodeRemote(hProcess, dllName, pLoadLibrary, pGetLastError, injectLibraryCodeRemote)) {
		DOUT("InitShellcodeRemote failed for (hProcess=[%p], dllName={%s}, pLoadLibrary=[%p], pGetLastError=[%p], injectLibraryCodeRemote=[%p])",
			hProcess, dllName, (LPCVOID)pLoadLibrary, (LPCVOID)pGetLastError, injectLibraryCodeRemote);
		ret = -1;
		goto __err_proc_mem_free;
	}

	DWORD exitCode = 0;
	if (RunMembufferRemote(hProcess, injectLibraryCodeRemote, sizeof(shellcode_t), 0, &exitCode)) {
		DOUT("RunMembufferRemote failed for (hProcess=[%p], getPLdrCodeRemote=[%p], size=%llu, dest=[%p], pexitCode=[%p])\n",
			hProcess, injectLibraryCodeRemote, sizeof(shellcode_t), NULL, &exitCode);
		ret = -1;
		goto __err_proc_mem_free;
	}

	if (exitCode) {
		ErrMsgEx("LoadLibrary", exitCode);
		ret = -1;
		goto __err_proc_mem_free;
	}

	if (!masterThreadCase)
		if (ResumeThread(hThread) == -1) {
			ErrMsg("ResumeThread for Application");
			goto __err_proc_mem_free;
		}

__err_proc_mem_free:
	if (!VirtualFreeEx(hProcess, injectLibraryCodeRemote, 0, MEM_RELEASE))
		ErrMsg("VirtualFreeEx");

__err_proc_free:
	if (ret)
		if (!TerminateProcess(hProcess, 0))
			ErrMsg("TerminateProcess");

	return ret;
}