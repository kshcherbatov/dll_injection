// DllInjector.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"
#include <stdio.h>
#include "RunDllInjectedThread.h"
#include "config.h"
#include <windows.h>

#include "getopt.h"

static void print_help_and_die(const char * const program_name, FILE * const stream, const int exit_code) {
	fprintf(stream,
		"Usage: %s [options]\n"
		"   -h   --help                       Display this usage information\n"
		"   -d   --dll           filename     Specify input DLL\n"
		"   -t   --target        filename     Specify target file\n",
		program_name
	);
	exit(exit_code);
}

static void parse_cmd_options_or_die(int argc, char *argv[], LPCSTR *dllname, LPCSTR *targetname) {
	const char * const short_options = "hd:t:";
	const struct option long_options[] = {
		{ "help",    0, NULL, 'h' },
		{ "dll",     1, NULL, 'd' },
		{ "target",  1, NULL, 't' },
		{ NULL,      0, NULL,  0 }
	};

	int ch = 0;
	while ((ch = getopt_long(argc, argv, short_options, long_options, NULL)) != -1) {
		switch (ch) {
		case 'h':
			print_help_and_die(argv[0], stdout, EXIT_SUCCESS);
		case 'd':
			*dllname = optarg;
			break;
		case 't':
			*targetname = optarg;
			break;
		default:
			print_help_and_die(argv[0], stderr, EXIT_FAILURE);
		}
	}
}


int main(int argc, char *argv[]) {
	LPCSTR prgName = NULL;
	LPCSTR dllName = NULL;

	parse_cmd_options_or_die(argc, argv, &dllName, &prgName);
	if (!dllName || !prgName) {
		fprintf(stderr, "No enougth input specified\n");
		exit(EXIT_FAILURE);
	}

	int injectionRes = RunLibraryInjectedProcess(prgName, dllName);
	printf("Load DLL %s.\n", (!injectionRes) ? "succeeded" : "failed");

	//test(&processInfo);
	return 0;
}
