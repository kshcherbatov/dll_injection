#pragma once

#include <windows.h>
#include <stdio.h>
#include <assert.h>

#define DEBUG 1
#ifdef DEBUG
#define DOUT printf
#else
#define DOUT if (0) printf
#endif // DE

void ErrMsgEx(LPCSTR msg, DWORD errCode) {
	CHAR errMsg[256];
	int errMsgLength = FormatMessageA(
		FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL, errCode,
		MAKELANGID(LANG_ENGLISH, SUBLANG_DEFAULT), (LPSTR)errMsg, 256, NULL
	);

	DOUT("%s: [%d] - %.*s", msg, errCode, errMsgLength, (LPCSTR)errMsg);
}

void ErrMsg(LPCSTR msg) {
	ErrMsgEx(msg, GetLastError());
}
