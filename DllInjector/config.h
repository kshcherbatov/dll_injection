#pragma once

#ifndef _WIN64
/* x32 target */
#define PRGNAME "C:\\Windows\\SysWOW64\\notepad.exe"
#define DLLNAME "D:\\WORK\\Study\\SysProg\\DllInjector\\Debug\\DxSNLogo.dll"
#else
/* x64 target */
//#define PRGNAME "D:\\WORK\\Study\\SysProg\\dll_injection\\DllInjector\\x64\\Debug\\d3dx9_test.exe"
//#define PRGNAME "D:\\Samples\\C++\\Direct3D\\Bin\\x64\\ShadowVolume.exe"
#define PRGNAME "D:\\Samples\\C++\\Direct3D\\Tutorials\\Tut06_Meshes\\x64\\Debug\\Meshes.exe"
//#define PRGNAME "D:\\Matrices.exe"
#define DLLNAME "D:\\WORK\\Study\\SysProg\\dll_injection\\DllInjector\\x64\\Debug\\DxSNLogo.dll"
#endif