// dllmain.cpp: ���������� ����� ����� ��� ���������� DLL.
#define CINTERFACE
#include "stdafx.h"
#include <d3d9.h>
#include "Hook.h"

Hooker hooker;

typedef LPDIRECT3D9(WINAPI *tDirect3DCreate9)(
	UINT);
typedef HRESULT (WINAPI *tD3DCreateDeviceFunction)(
	LPDIRECT3D9 _this,
	UINT Adapter, D3DDEVTYPE DeviceType, HWND hFocusWindow,
	DWORD BehaviorFlags, D3DPRESENT_PARAMETERS* pPresentationParameters,
	IDirect3DDevice9** ppReturnedDeviceInterface);
typedef HRESULT (WINAPI *tD3DDeviceEndScene)(
	LPDIRECT3DDEVICE9 _this);


struct CUSTOMVERTEX { FLOAT X, Y, Z, RHW; DWORD COLOR; };
#define CUSTOMFVF (D3DFVF_XYZRHW | D3DFVF_DIFFUSE)
LPDIRECT3DVERTEXBUFFER9 v_buffer = NULL;

void InitGraphics(LPDIRECT3DDEVICE9 d3ddev) 
{
	CUSTOMVERTEX vertices[] =
	{
		{ 200.0f, 100.25f, 0.3f, 1.0f, D3DCOLOR_XRGB(0, 0, 255), },
		{ 325.0f, 250.0f, 0.3f, 1.0f, D3DCOLOR_XRGB(0, 255, 0), },
		{ 75.0f, 250.0f, 0.3f, 1.0f, D3DCOLOR_XRGB(255, 0, 0), },
		{ 200.0f, 400.0f, 0.3f, 1.0f, D3DCOLOR_XRGB(255, 255, 255), },
	};

	d3ddev->lpVtbl->CreateVertexBuffer(d3ddev, 4 * sizeof(CUSTOMVERTEX),
		0,
		CUSTOMFVF,
		D3DPOOL_MANAGED,
		&v_buffer,
		NULL);

	VOID* pVoid;
	v_buffer->lpVtbl->Lock(v_buffer, 0, 0, (void**)&pVoid, 0);
	memcpy(pVoid, vertices, sizeof(vertices));
	v_buffer->lpVtbl->Unlock(v_buffer);
}


HRESULT WINAPI HookedD3DDeviceEndScene(LPDIRECT3DDEVICE9 _this) 
{
	hooker.logIt("Called HookedD3DDeviceEndScene");
	tD3DDeviceEndScene pD3DDeviceEndScene = (tD3DDeviceEndScene)hooker.getOrinalFunc(HookedD3DDeviceEndScene);

	_this->lpVtbl->SetFVF(_this, CUSTOMFVF);
	_this->lpVtbl->SetStreamSource(_this, 0, v_buffer, 0, sizeof(CUSTOMVERTEX));
	_this->lpVtbl->DrawPrimitive(_this, D3DPT_TRIANGLESTRIP, 0, 2);

	return pD3DDeviceEndScene(_this);
}


HRESULT WINAPI HookedD3DCreateDeviceFunction(
	LPDIRECT3D9 _this,
	UINT Adapter, D3DDEVTYPE DeviceType, HWND hFocusWindow,
	DWORD BehaviorFlags, D3DPRESENT_PARAMETERS* pPresentationParameters,
	IDirect3DDevice9** ppReturnedDeviceInterface)
{
	hooker.logIt("Called HookedD3DCreateDeviceFunction");
	tD3DCreateDeviceFunction pDirect3DCreate9 = (tD3DCreateDeviceFunction)hooker.getOrinalFunc(HookedD3DCreateDeviceFunction);

	LPDIRECT3DDEVICE9 d3dDev = NULL;
	HRESULT ret = pDirect3DCreate9(_this, Adapter, DeviceType, hFocusWindow, BehaviorFlags,
		pPresentationParameters, &d3dDev);
	*ppReturnedDeviceInterface = d3dDev;

	hooker.setupHook((LPVOID *)&d3dDev->lpVtbl->EndScene, HookedD3DDeviceEndScene);

	InitGraphics(d3dDev);
	
	return ret;
}


LPDIRECT3D9 WINAPI HookedDirect3DCreate9(UINT SDKVersion) 
{
	hooker.logIt("Called HookedDirect3DCreate9");
	tDirect3DCreate9 pDirect3DCreate9 = (tDirect3DCreate9)hooker.getOrinalFunc(HookedDirect3DCreate9);
		
	LPDIRECT3D9 d3d = NULL;
	d3d = pDirect3DCreate9(SDKVersion);

	hooker.setupHook((LPVOID *)&d3d->lpVtbl->CreateDevice, HookedD3DCreateDeviceFunction);

	return d3d;
}


BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
					 )
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
		hooker.setupHook("Direct3DCreate9", HookedDirect3DCreate9);
		break;

	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}
	return TRUE;
}