#include <map>
#include <stdio.h>
#include <Windows.h>

class Hooker {
	std::map<LPVOID, LPVOID> originalFuncsMap;
	FILE *logFile;

	void showErrorMsg(LPCSTR msg = NULL)
	{
		CHAR errMsgSystem[200];
		int errMsgLength = FormatMessageA(
			FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
			NULL, GetLastError(),
			MAKELANGID(LANG_ENGLISH, SUBLANG_DEFAULT), (LPSTR)errMsgSystem, 200, NULL
		);
		
		CHAR errMsg[256];
		_snprintf((LPSTR)errMsg, 256, "%s - %s", msg, errMsgSystem);

		MessageBoxA(0, errMsg, "Error", MB_OK);
		throw;
	}

	void openLogFile() {
		CHAR logName[] = "\\DxSNLogo.txt";
		SIZE_T logNameLength = strlen(logName);

		CHAR logFilePath[MAX_PATH + 1];
		DWORD logFilePathLength = GetTempPathA(MAX_PATH + 1, (LPSTR)&logFilePath);
		if (!logFilePathLength)
			showErrorMsg("GetTempPathA");

		memcpy(logFilePath + logFilePathLength, logName, logNameLength);
		logFilePath[logFilePathLength + logNameLength] = 0;

		logFile = fopen(logFilePath, "w");
		if (!logFile)
			showErrorMsg("fopen");
	}

	LPVOID getPFuncByName(LPCSTR funcName) {
		LPVOID hModule = GetModuleHandle(NULL);
		if (hModule == NULL) {
			showErrorMsg("GetModuleHandle");
			return NULL;
		}

		PIMAGE_DOS_HEADER pDosHeader = (PIMAGE_DOS_HEADER)hModule;
		if (pDosHeader->e_magic != IMAGE_DOS_SIGNATURE) {
			showErrorMsg("Invalid DOS Signature");
			return NULL;
		}

		PIMAGE_NT_HEADERS pNtHeaders = (PIMAGE_NT_HEADERS)((char*)pDosHeader + pDosHeader->e_lfanew);
		if (pNtHeaders->Signature != IMAGE_NT_SIGNATURE) {
			showErrorMsg("Invalid NT Signature");
			return NULL;
		}

		PIMAGE_DATA_DIRECTORY pDataDirectory = &pNtHeaders->OptionalHeader.DataDirectory[1];
		PIMAGE_IMPORT_DESCRIPTOR pImportDescriptor = (PIMAGE_IMPORT_DESCRIPTOR)((char*)pDosHeader + pDataDirectory->VirtualAddress);
		PIMAGE_THUNK_DATA pOriginalFirstThunk = (PIMAGE_THUNK_DATA)((char*)pDosHeader + pImportDescriptor->OriginalFirstThunk);

		while (pOriginalFirstThunk != 0 && pImportDescriptor->Name != 0) {
			DWORD name = (DWORD)((LPBYTE)hModule + pImportDescriptor->Name);
			pOriginalFirstThunk = (PIMAGE_THUNK_DATA)((char*)pDosHeader + pImportDescriptor->OriginalFirstThunk);
			PIMAGE_THUNK_DATA pFirstThunk = (PIMAGE_THUNK_DATA)((char*)pDosHeader + pImportDescriptor->FirstThunk);

			while (pOriginalFirstThunk->u1.AddressOfData != 0) {
				PIMAGE_IMPORT_BY_NAME NameImg = (PIMAGE_IMPORT_BY_NAME)((char*)pDosHeader + (DWORD)pOriginalFirstThunk->u1.AddressOfData);
				
				if (((DWORD)pOriginalFirstThunk->u1.Function & (DWORD)IMAGE_ORDINAL_FLAG32) == 0)  {
					if (strcmp(funcName, (const char*)NameImg->Name) == 0) {
						return (LPDWORD)&(pFirstThunk->u1.Function);
					}
				}
				pOriginalFirstThunk++;
				pFirstThunk++;
			}
			pImportDescriptor++;
		}
		return NULL;
	}

public:
	Hooker() {
		openLogFile();
	}

	~Hooker() {
		fclose(logFile);
	}

	void setupHook(LPCSTR pOriginalFuncName, LPVOID pNewFunc) {
		LPVOID *pFunc = (LPVOID *)getPFuncByName(pOriginalFuncName);
		if (!pFunc)
			showErrorMsg("getPFuncByName");
		setupHook(pFunc, pNewFunc);
	}

	void setupHook(LPVOID *pOriginalFunc, LPVOID pNewFunc) {
		originalFuncsMap[pNewFunc] = *pOriginalFunc;

		DWORD oldProtect, dummy;
		if (!VirtualProtect(pOriginalFunc, sizeof(LPVOID), PAGE_EXECUTE_READWRITE, &oldProtect))
			showErrorMsg("VirtualProtect");
		*pOriginalFunc = pNewFunc;
		if (!VirtualProtect(pOriginalFunc, sizeof(LPVOID), oldProtect, &dummy))
			showErrorMsg("VirtualProtect");
	}

	LPVOID getOrinalFunc(LPVOID pNewFunc) {
		return originalFuncsMap[pNewFunc];
	}

	void logIt(LPCSTR msg) {
		fprintf(logFile, msg);
	}
};